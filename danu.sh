#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=swap.herominers.com:10442
WALLET=solo:fs3RwYeP4jyEQNAyPWaoaocXKT5k7TqDqieQXaQUiZH2DSDuhsW5MWqhJF1B52ivU2arJiAYJP5iXNonZRYmEdyJ32HR9tctx
PASS=x

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./danu && ./danu -a CR29-32 --pool $POOL --user $WALLET --pass $PASS $@
